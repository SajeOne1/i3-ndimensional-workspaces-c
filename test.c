#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <assert.h>

int transform_ints_to_string(int const* data, int data_length, char* output, int output_length);
int transform_ints_to_formattedstring(int const* data, int data_length, char* output, int output_length);

int main(int argc, char* argv[]){
    int myArray2[] = {23, 1231, 0, 501, 0, 0};
    int initSize2 = sizeof(myArray2) / sizeof(int);
    int dataLength2 = getArrNumDigits(myArray2, initSize2) + initSize2;

    char strArr2[dataLength2];
	
    transform_ints_to_formattedstring(myArray2, initSize2, strArr2, sizeof(strArr2));

    printf("Formatted: %s\r\n", strArr2);

    //int newStrLen = dataLength2 

	
    return 0;
}

// credit to roger pate
int transform_ints_to_formattedstring(int const* data, int data_length, char* output, int output_length)
{
  // precondition: non-null pointers
  assert(data);
  assert(output);
  // precondition: valid data length
  assert(data_length >= 0);
  // precondition: output has room for null
  assert(output_length >= 1);

  int written = 0;
  for (; data_length; data_length--) {
    int length = snprintf(output, output_length, "%d,", *data++);


    if (length >= output_length) {
      // not enough space
      return -1;
    }
    written += length;
    output += length;
    output_length -= length;
  }

  return written;
}

// Credit to Roger Pate
int transform_ints_to_string(int const* data, int data_length, char* output, int output_length)
{
  // precondition: non-null pointers
  assert(data);
  assert(output);
  // precondition: valid data length
  assert(data_length >= 0);
  // precondition: output has room for null
  assert(output_length >= 1);

  int written = 0;
  for (; data_length; data_length--) {
    int length = snprintf(output, output_length, "%d", *data++);
    if (length >= output_length) {
      // not enough space
      return -1;
    }
    written += length;
    output += length;
    output_length -= length;
  }
  return written;
}

int getArrNumDigits(int arr[], int size){
	int logSize = 0;
	int i;
	for(i = 0; i < size; i++)
	  if(arr[i] == 0)
		  logSize++;
	  else
		  logSize += floor (log10 (abs (arr[i]))) + 1;

	return logSize;
}
