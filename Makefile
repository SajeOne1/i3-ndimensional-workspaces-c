# Set up basic variables:
CC = gcc
CFLAGS = -Wall -g -lm
LDFLAGS =
 
# List of sources:
SOURCES = main.c
OUT = i3nd
 
LDFLAGS += `pkg-config --libs --cflags i3ipc-glib-1.0`
 
i3nd: main.c utilities.c
	$(CC) $(SOURCES) -o $(OUT) $(CFLAGS) $(LDFLAGS)

test:
	$(CC) test.c -o testing $(CFLAGS) $(LDFLAGS)
