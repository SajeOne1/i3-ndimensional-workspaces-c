//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
// Author: Shane Brown
// Date: 19/03/2016
// Revision: 1
// Description: Adds multi-dimensional workspaces to i3wm
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int get_nonnumeric_index(const char* str);
void to_vector(int vec[], const char* str);
int indexOf(char needle, const char* haystack);
int stringToInt(char* str);

int get_nonnumeric_index(const char* str){
	for(int i = 0; i < strlen(str); i++){
		if(str[i]-'0' > 9 || str[i]-'0' < 0){
			return i;
		}
	}

	return -1;
}

// Turns a vector string into a vector array
void to_vector(int vec[], const char* str){
    char x[5];
    char y[5];

    int colonIndex = indexOf(',', str);
    if(colonIndex == -1){
        vec[0] = atoi(str);
        vec[1] = 1;
        return;
    }

    int count = 0;
    for(int i = 0; i < strlen(str); i++){
        if(i < colonIndex){
            x[i] = str[i];
            x[i+1] = '\0';
        }else if(i > colonIndex){
            y[count] = str[i];
            y[count+1] = '\0';
            count++;
        }
    }

	printf("x: %s\n", x);
    vec[0] = atoi(x);
    vec[1] = atoi(y);
}

int serializeVector(int x, int y, char* output){
	char buff[5];
	int length = 0;

	printf("Serialize: (%d, %d)\n", x, y);

	if(y != 1){
		char template[] = "%d,%d";
		sprintf(buff, template, x, y);
	}else{
		printf("i think it's single\n");
		char template[] = "%d";
		sprintf(buff, template, x);
	}
	printf("buff: %s\n", buff);

	length = strlen(buff);
	printf("len(buff): %d\n", length);
	strncpy(output, buff, length);
	output[length] = '\0';
	printf("output: %s\n", output);
	printf("len(output): %ld\n", strlen(output));
	return length;
}

int indexOf(char needle, const char* haystack){
    const char* ptr = strchr(haystack, needle);

    if(ptr){
        return ptr - haystack;
    }

    return -1;
}

int stringToInt(char* str){
    int intVal;
    char *ptr;

    intVal = strtol(str, &ptr, 10);

    if(*ptr == '\n' || *ptr == '\0'){
        return intVal;
    }

    return 0;
}
