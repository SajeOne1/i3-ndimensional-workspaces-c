//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
// Author: Shane "SajeOne" Brown
// Date: 19/03/2016
// Revision: 1
// Description: Adds 2-dimensional workspaces to i3wm
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

#include <i3ipc-glib/i3ipc-glib.h>
#include <getopt.h> // Argument parsing
#include "utilities.c"

// Func Declarations
void moveVertRelative(int up, int transport);
void printVertInfo();
gchar* getFocusedWorkspaceName(i3ipcConnection* conn);
gchar** getAllWorkspaces(i3ipcConnection* conn);
char* parseArgs(int argc, char* argv[], int* op, int* transport);

int translate(int w, int dimension, int transport);

gint main(int argc, char* argv[]) {
    int op = -1;
    int transport = 0;
    char* value = parseArgs(argc, argv, &op, &transport);
    int intVal = 0;

    switch(op){
       case 1: // Vertical
            intVal = stringToInt(value);
			if(transport){
				translate(intVal, 2, 1);
			}
			else{
				translate(intVal, 2, 0);
			}
            break;
        case 2: // Horizontal
            intVal = stringToInt(value);
            if(intVal != 0){
                if(transport){
					translate(intVal, 1, 1);
				}else{
					translate(intVal, 1, 0);
				}
            }else{
                puts("Invalid column passed");
			}
			break;
		case 3: // Up
			printf("up\n");
			moveVertRelative(1, transport);
			break;
		case 4: // Down
			printf("down\n");
			moveVertRelative(0, transport);
		case 5: // Print vertical info
			printVertInfo();
    }

    return 0;
}

void moveVertRelative(int up, int transport){
	i3ipcConnection *conn;
	conn = i3ipc_connection_new(NULL, NULL);
	gchar* focused = getFocusedWorkspaceName(conn);
	printf("focused: %s\n", focused);

	int curSpace[2];
	to_vector(curSpace, focused);

	if(up)
		curSpace[1]++;
	else
		curSpace[1]--;

	printf("newVert Vec: (%d, %d)\n", curSpace[0], curSpace[1]);
	translate(curSpace[1], 2, transport);
}

int translate(int w, int dimension, int transport){
    // Create i3ipc Connection
    i3ipcConnection *conn;
    conn = i3ipc_connection_new(NULL, NULL);

    // Get Focused Workspace
    gchar* focused = getFocusedWorkspaceName(conn);

    if(focused == NULL)
        puts("Focused WS is NULL");

    // Calculate desired workspace vector
	int curSpace[2];
	to_vector(curSpace, focused);

	int next_ws_vector[2];
	if(dimension == 1){ // Moving horizontally
		next_ws_vector[0] = w;
		next_ws_vector[1] = curSpace[1];
	}
	else if(dimension == 2){ // Moving vertically
		next_ws_vector[0] = curSpace[0];
		next_ws_vector[1] = w;
	}

    // Used for string concat
    char* workspaceConst = "workspace \"%s\"";
    if(transport){
        workspaceConst = "move container to workspace \"%s\"";
    }

	char* workspaceDest = NULL;

	// Isolate any non-coordinate characters from workspace name
	int start = get_nonnumeric_index(focused);
	printf("start: %d\n", start);

	char* special_chars = NULL;
	if(start > -1 && focused[start] != ','){ // non-coord is our delimeter, ignore
		int end = indexOf(',', focused);
		if(end == -1){ // If we are on vertical index 1, non-coords continue until end of name
			end = strlen(focused)-1;
		}else{
			end--; // Don't include ','
		}

		int focused_len = strlen(focused);
		special_chars = malloc(sizeof(char) * (focused_len + 1));

		int k = 0;
		for(int i = start; i <= end; i++){
			special_chars[k++] = focused[i];
		}
		special_chars[k] = '\0';

		printf("special_chars: %s\n", special_chars);
	}

	if(special_chars != NULL){
		if(next_ws_vector[1] == 1){ // doesn't need comma
			char* template = "%d%s";
			int dest_size = sizeof(char) * (strlen(template) + strlen(special_chars) + 3 + 1);
			workspaceDest = malloc(dest_size);
			snprintf(workspaceDest, dest_size, template, next_ws_vector[0], special_chars);
		}else{
			char* template = "%d%s,%d";
			int dest_size = sizeof(char) * (strlen(template) + strlen(special_chars) + 3 + 3 + 1);
			workspaceDest = malloc(dest_size);
			snprintf(workspaceDest, dest_size, template, next_ws_vector[0], special_chars, next_ws_vector[1]);
		}
	}else{ // If no special chars, perform regular serialize
		workspaceDest = malloc(sizeof(char) * 5);
		serializeVector(next_ws_vector[0], next_ws_vector[1], workspaceDest);
	}

	printf("next_ws_vector[0]: %d, next_ws_vector[1]: %d\n", next_ws_vector[0], next_ws_vector[1]);
	printf("workspace destination: %s\n", workspaceDest);

	int command_size = sizeof(char) * (strlen(workspaceConst) + strlen(workspaceDest) + 1);
	char* command = malloc(command_size);
	snprintf(command, command_size, workspaceConst, workspaceDest);

    printf("i3ipc: %s\n", command);

    i3ipc_connection_command(conn, command, NULL);

    // Garbage Collection
	free(command);
	free(special_chars);
    g_object_unref(conn);

    return 0;
}

/* UTILITIES */
char* parseArgs(int argc, char* argv[], int *op, int *transport){
    int c;
    char* value;

    while((c = getopt(argc, argv, "ptudv:h:")) != -1){
        switch(c){
            case 't':
                *transport = 1;
                break;
            case 'v':
                value = malloc(sizeof(optarg));
                strcpy(value, optarg);
                *op = 1;
                break;
            case 'h':
                value = malloc(sizeof(optarg));
                strcpy(value, optarg);
                *op = 2;
                break;
			case 'u':
				*op = 3;
				break;
			case 'd':
				*op = 4;
				break;
			case 'p':
				*op = 5;
				break;
            default:
                puts("usage: [-p print] [-t transport] [-u up] [-d down] [-v] row [-h] col");
                return 0x0;
        }
    }

    return value;
}

gchar** getAllWorkspaces(i3ipcConnection* conn){
	i3ipcCon* tree = i3ipc_connection_get_tree (conn, NULL);
	GList* workspaces = i3ipc_con_workspaces(tree);
	guint length = g_list_length(workspaces);

	char** retSpaces = malloc((length + 1) * sizeof(char*));

	int i;
	for(i = 0; i < length; i++){
		i3ipcCon* curSpace = (i3ipcCon*)g_list_nth_data(workspaces, i); 
		gchar* curName = (gchar*)i3ipc_con_get_name(curSpace);

		retSpaces[i] = malloc(sizeof(curName));

		strcpy(retSpaces[i], curName);
	}
    retSpaces[length] = '\0';

    return retSpaces;
}

void printVertInfo(){
	// Create i3ipc Connection
    i3ipcConnection *conn;
	int vert;

    conn = i3ipc_connection_new(NULL, NULL);


    // Get Focused Workspace
    gchar* focused = getFocusedWorkspaceName(conn);

    if(focused == NULL){
        puts("Focused WS is NULL");
		return;
	}

	vert = 1;
	strtok(focused, ",");
	char* tok = strtok(NULL, ",");
	if(tok != NULL){
		vert = atoi(tok);
	}

	printf("[%d]: ", vert);

	i3ipcCon* tree = i3ipc_connection_get_tree (conn, NULL);
	GList* workspaces = i3ipc_con_workspaces(tree);
	guint length = g_list_length(workspaces);

	int vertList[length];

	int vListIter = 0;
	for(int i = 0; i < length; i++){
		i3ipcCon* curSpace = (i3ipcCon*)g_list_nth_data(workspaces, i);
		int tempVec[2];
		const gchar* name = i3ipc_con_get_name(curSpace);
		to_vector(tempVec, name);
		int exists = 0;
		for(int k = 0; k <= i; k++){
			if(tempVec[1] == vertList[k]){
				exists = 1;
				break;
			}
		}

		if(exists)
			continue;

		vertList[vListIter] = tempVec[1];
		vListIter++;

	}

	for(int i = 0; i < vListIter; i++){
		if(i == 0)
			printf("%d", vertList[i]);
		else
			printf(", %d", vertList[i]);
	}

	printf("\n");
}

gchar* getFocusedWorkspaceName(i3ipcConnection* conn){
    if(conn != NULL){
		i3ipcCon* tree = i3ipc_connection_get_tree (conn, NULL);
		i3ipcCon* focused = i3ipc_con_find_focused(tree);

        const gchar* focusedName = i3ipc_con_get_name(focused);
		i3ipcCon* workspace = i3ipc_con_workspace (focused);

        char* retName;
        if(workspace != NULL){
            gchar* name = (gchar*) i3ipc_con_get_name (workspace);
            retName = (gchar*) malloc(sizeof(name));
            strcpy(retName, name);
        }else{
            retName = (gchar*) malloc(sizeof(focusedName));
            strcpy(retName, focusedName);
        }

        return retName;
    }

    return NULL;
}
